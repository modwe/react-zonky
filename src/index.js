/* eslint-disable */
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { ConnectedRouter } from 'react-router-redux';
import { Route, Switch } from "react-router-dom";
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import './index.css';
import Loans from './Containers/Loans';
import LoanDetail from './Containers/LoanDetail';
import registerServiceWorker from './registerServiceWorker';
import store, { history } from './Redux/Store';
import Error404 from './Components/Error404';



ReactDOM.render(
<Provider store={store}>
  <ConnectedRouter history={history}>
    <Switch>
      <Route exact path="/" component={Loans}/>
      <Route path="/detail/:id" component={LoanDetail} />
      <Route component={Error404} />
    </Switch>
  </ConnectedRouter>
</Provider>, document.getElementById('root'));
registerServiceWorker();
