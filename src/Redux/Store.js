import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
import createHistory from 'history/createBrowserHistory';
import { routerReducer } from 'react-router-redux';
import thunkMiddleware from 'redux-thunk';

import { LoansReducer, loansStartInterval } from './Reducers/LoansReducer';

/* eslint-disable*/
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;
/* eslint-enable */

const store = createStore(
  combineReducers({
    LoansReducer,
    routing: routerReducer,
  }),
  composeEnhancers(applyMiddleware(thunkMiddleware)),
);

store.dispatch(loansStartInterval());

export const history = createHistory();

export default store;
