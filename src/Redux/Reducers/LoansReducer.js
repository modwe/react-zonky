import ApiClient from '../../Utils/ApiClient';

export const LOANS_FETCH = 'LOANS_FETCH';
export const LOANS_SUCCESS = 'LOANS_SUCCESS';
export const LOANS_INTERVAL = 'LOANS_INTERVAL';
export const LOANS_ERROR = 'LOANS_ERROR';
export const LOANS_SORT_BY = 'LOANS_SORT_BY';

const initialState = {
  loans: null,
  isFetching: false,
  error: null,
  sortBy: 'rating',
  orientation: 'DSC',
  interval: null,
  sorters: [
    {
      title: 'hodnoty',
      parameter: 'amount',
    },
    {
      title: 'hodnotenia',
      parameter: 'rating',
    },
    {
      title: 'deadlinu',
      parameter: 'deadline',
    },
    {
      title: 'délky',
      parameter: 'termInMonths',
    },
  ],
};

export function LoansReducer(state = initialState, action) {
  switch (action.type) {
    case LOANS_FETCH:
      return {
        ...state, loans: null, isFetching: true, error: null,
      };
    case LOANS_ERROR:
      return {
        ...state,
        loans: null,
        isFetching: false,
        error: action.payload,
      };
    case LOANS_SUCCESS:
      return {
        ...state,
        loans: action.payload,
        isFetching: false,
        error: null,
      };
    case LOANS_INTERVAL:
      return {
        ...state,
        interval: action.payload,
      };
    case LOANS_SORT_BY:
      return { ...state, sortBy: action.payload.by, orientation: action.payload.orientation };
    default:
      return state;
  }
}

export function loansFetch() {
  return {
    type: LOANS_FETCH,
  };
}

export function loansError(error) {
  return {
    type: LOANS_ERROR,
    payload: error,
  };
}

export function loansSuccess(payload) {
  return {
    type: LOANS_SUCCESS,
    payload,
  };
}

export function loansGet() {
  return (dispatch) => {
    dispatch(loansFetch());
    ApiClient.getLoans()
      .then(res => dispatch(loansSuccess(res.data)))
      .catch(error => dispatch(loansError(error)));
  };
}

export function loansSortBy(payload) {
  return {
    type: LOANS_SORT_BY,
    payload,
  };
}

export function loansInterval(payload) {
  return {
    type: LOANS_INTERVAL,
    payload,
  };
}

export function loansStartInterval() {
  return (dispatch, getState) => {
    if (getState().interval != null) {
      return;
    }
    dispatch(loansGet());
    const intervalId = setInterval(() => {
      dispatch(loansGet());
    }, 300000);
    dispatch(loansInterval(intervalId));
  };
}
