import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Error404 from '../Components/Error404';
import Spinner from '../Components/Spinner';
import { propTypes as loanPropTypes } from '../Components/Loan';
import Detail from '../Components/Detail';


const propTypes = {
  ...loanPropTypes,
  isFetching: PropTypes.bool,
};

/* eslint-disable */
const defaultProps = {
  loan: null,
  isFetching: false,
};
/* eslint-enable */

function LoanDetail({ loan, isFetching }) {
  if (isFetching) {
    return <Spinner />;
  }

  if (!isFetching && !loan) {
    return <Error404 />;
  }

  return (
    <Detail loan={loan} />
  );
}

LoanDetail.propTypes = propTypes;
LoanDetail.defaultProps = defaultProps;

function mapStateToProps(state, { match }) {
  return {
    loan: state.LoansReducer.loans
      ? [...state.LoansReducer.loans].find(el => el.id === +match.params.id)
      : null,
    isFetching: state.LoansReducer.isFetching,
  };
}

export default connect(mapStateToProps)(LoanDetail);
