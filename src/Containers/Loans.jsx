import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';
import LoanList, {
  propTypes as LoanListPropTypes,
} from '../Components/LoanList';
import { sort } from '../Utils/Helpers';
import { loansSortBy } from '../Redux/Reducers/LoansReducer';
import SorterList from '../Components/SorterList';
import Spinner from '../Components/Spinner';


const propTypes = {
  loans: LoanListPropTypes.loans,
  loansSortBy: PropTypes.func,
};
const defaultProps = {
  loans: null,
  loansSortBy: null,
};

function Loans({ loans, loansSortBy, sorters, sorter, isFetching }) {

  const getLoans = () => {
    if (isFetching) {
      return (
        <Spinner />
      );
    }

    return (
      <div className="loanContainer">
        <LoanList loans={loans} />
      </div>
    );
  }

  return (
    <ReactCSSTransitionGroup
      transitionAppear
      transitionAppearTimeout={600}
      transitionEnterTimeout={600}
      transitionLeaveTimeout={200}
      transitionName="SlideIn"
    >
      <div>
        <SorterList sorter={sorter} parameters={sorters} action={loansSortBy} />
        {getLoans()}
      </div>
    </ReactCSSTransitionGroup>
  );
}

Loans.propTypes = propTypes;
Loans.defaultProps = defaultProps;

function mapStateToProps(state) {
  return {
    isFetching: state.LoansReducer.isFetching,
    loans: sort(
      state.LoansReducer.loans,
      state.LoansReducer.sortBy,
      state.LoansReducer.orientation,
    ),
    sorters: state.LoansReducer.sorters,
    sorter: {
      sortBy: state.LoansReducer.sortBy,
      orientation: state.LoansReducer.orientation,
    },
  };
}

function mapDispatchToProps(dispatch) {
  return {
    loansSortBy: (params) => {
      dispatch(loansSortBy(params));
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(Loans);
