/* eslint-disable */
import { getZs, truncate } from '../Utils/Helpers';
import ApiClient from '../Utils/ApiClient';

test('expect length 5 and correct form', () => {
  const s = 'A';
  expect((s + getZs(5 - s.length)).length).toBe(5);
  expect(s + getZs(5 - s.length)).toBe('AZZZZ');
});

test('expect string to have max 200 chars and max 20 words', () => {
  const s = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc quis hendrerit lectus. Proin eu dapibus magna, nec consectetur ipsum. Nullam facilisis vulputate lectus id scelerisque. In rhoncus, lorem sed vehicula condimentum, massa quam iaculis libero, eu tristique elit neque interdum justo. Duis egestas consequat lectus, in elementum elit interdum et. Nullam a quam eros. Phasellus vestibulum nulla vel nisl dictum interdum. Integer lobortis risus leo, vel congue ipsum maximus vitae. Aliquam in odio nisi. Quisque lacinia ultricies sapien at feugiat. In et porta nisi. Phasellus eget nibh congue, ornare enim in, finibus est. Suspendisse vel eros vitae magna pellentesque convallis et rutrum justo. Suspendisse sed est lorem. Nunc arcu arcu, vehicula eget odio at, maximus euismod arcu.';
  const control = truncate(s).length;
  expect(control).toBeLessThanOrEqual(200);
  expect((`${control}`).split(' ').length).toBeLessThanOrEqual(200);
});

test('Except api response with statuscode 200', () => {
  expect.assertions(1);
  return ApiClient.getLoans().then((data) => {
    expect(data.status).toBe(200);
  });
});
