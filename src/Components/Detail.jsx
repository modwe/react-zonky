import React from 'react';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import BackButton from '../Components/BackButton';
import LoanTimeBlock from '../Components/LoanTimeBlock';
import LoanValueIndicator from '../Components/LoanValueIndicator';
import { propTypes as loanPropTypes } from '../Components/Loan';

const propTypes = {
  loan: loanPropTypes.loan.isRequired,
};


export default function Detail({ loan }) {
  const loanValueIndicatorProps = {
    amount: loan.amount,
    remainingInvestment: loan.remainingInvestment,
    investmentRate: loan.investmentRate,
    termInMonths: loan.termInMonths,
    interestRate: loan.interestRate,
  };

  return (
    <ReactCSSTransitionGroup
      transitionAppear
      transitionAppearTimeout={600}
      transitionEnterTimeout={600}
      transitionLeaveTimeout={200}
      transitionName="SlideIn"
    >
      <div className="loanDetailWrapper">
        <h1>{loan.name}</h1>
        <div className="loanDetail">
          <div className="row">
            <div className="leftCol">
              <img
                src={`https://api.zonky.cz${loan.photos[0].url}`}
                alt={loan.name}
              />
              <span className="loanRating">{loan.rating}</span>
            </div>
            <div className="rightCol">
              <span className="block nickName big">{loan.nickName}</span>
              <LoanValueIndicator {...loanValueIndicatorProps} />
              <p>{loan.story}</p>
            </div>
          </div>
          <LoanTimeBlock
            sDatePublished={loan.datePublished}
            sDeadline={loan.deadline}
          />
        </div>
      </div>
      <div className="text-center">
        <BackButton />
      </div>
    </ReactCSSTransitionGroup>
  );
}

Detail.propTypes = propTypes;
