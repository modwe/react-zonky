import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  percentage: PropTypes.number,
};

const defaultProps = {
  percentage: 0,
};

export default function PercentageIndicator({ percentage }) {
  return (
    <div className="percentageIndicator">
      <div
        className="percentageIndicatorValue"
        style={{ width: `${percentage * 100}%` }}
      />
    </div>
  );
}

PercentageIndicator.propTypes = propTypes;
PercentageIndicator.defaultProps = defaultProps;
