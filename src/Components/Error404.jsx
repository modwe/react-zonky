import React from 'react';

export default function Error404() {
  return (
    <div className="errorWrapper">
      <h1>Ooops 404!</h1>
      <p>Sorry, the page you are looking for could not be found.</p>
    </div>
  );
}
