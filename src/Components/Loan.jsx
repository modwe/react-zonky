import React from 'react';
import PropTypes from 'prop-types';
import { Link, withRouter } from 'react-router-dom';

import { truncate } from '../Utils/Helpers';

export const propTypes = {
  loan: PropTypes.shape({
    id: PropTypes.number,
    url: PropTypes.string,
    name: PropTypes.string,
    story: PropTypes.string,
    purpose: PropTypes.string,
    photos: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string,
      url: PropTypes.string,
    })),
    nickName: PropTypes.string,
    termInMonths: PropTypes.number,
    interestRate: PropTypes.number,
    rating: PropTypes.string,
    topped: PropTypes.bool,
    amount: PropTypes.number,
    remainingInvestment: PropTypes.number,
    investmentRate: PropTypes.number,
    covered: PropTypes.bool,
    datepublished: PropTypes.string,
    published: PropTypes.bool,
    deadline: PropTypes.string,
    investmentsCount: PropTypes.number,
    questionsCount: PropTypes.number,
    region: PropTypes.string,
    mainIncomeType: PropTypes.string,
    insuranceActive: PropTypes.bool,
    insuranceHistory: PropTypes.arrayOf(PropTypes.shape({
      policyPeriodFrom: PropTypes.string,
      policyPeriodTo: PropTypes.string,
    })),
  }),
};

const defaultProps = {
  loan: null,
};

function Loan({ loan }) {
  const deadline = new Date(loan.deadline);

  return (
    <div className="loan">
      <div className="popupContainer">
        <img src={`https://api.zonky.cz${loan.photos[0].url}`} alt={loan.name} />
        <div className="popup">
          <span className="loanDeadline">Deadline {`${deadline.getDate()}.${deadline.getMonth() + 1}.${deadline.getFullYear()} ${deadline.getHours()}:${deadline.getMinutes()}`}</span>
          <span className="loanDuration">Délka splácení {loan.termInMonths} měsíců</span>
        </div>
      </div>
      <span className="loanName">{loan.name}</span>
      <span className="loanAmount">{`${loan.amount} Kč`}</span>
      <span className="loanRating">{loan.rating}</span>
      <p className="loanStory">{loan.story && truncate(loan.story)}</p>
      <Link to={`/detail/${loan.id}`} className="loanButton">Detail</Link>
    </div>
  );
}

Loan.propTypes = propTypes;
Loan.defaultProps = defaultProps;

export default withRouter(Loan);
