import React from 'react';
import { Link, withRouter } from 'react-router-dom';

function BackButton() {
  return (
    <Link to="/" className="backButton">Domov</Link>
  );
}

export default withRouter(BackButton);
