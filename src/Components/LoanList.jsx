import React from 'react';
import PropTypes from 'prop-types';
import Loan, { propTypes as LoanProps } from './Loan';

export const propTypes = {
  loans: PropTypes.arrayOf(LoanProps.loan),
};

const defaultProps = {
  loans: null,
};

export default function LoanList({ loans }) {
  if (!loans) {
    return null;
  }

  return loans.map(loan => (
    <Loan key={loan.id} loan={loan} />
  ));
}

LoanList.propTypes = propTypes;
LoanList.defaultProps = defaultProps;
