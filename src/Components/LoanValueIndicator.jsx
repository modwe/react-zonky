import React from 'react';
import PropTypes from 'prop-types';
import PercentageIndicator from './PercentageIndicator';

const propTypes = {
  amount: PropTypes.number.isRequired,
  remainingInvestment: PropTypes.number.isRequired,
  investmentRate: PropTypes.number.isRequired,
  termInMonths: PropTypes.number.isRequired,
  interestRate: PropTypes.number.isRequired,
};


export default function LoanValueIndicator({
  amount,
  remainingInvestment,
  interestRate,
  investmentRate,
  termInMonths,
}) {
  return (
    <React.Fragment>
      <div className="text-center">
        <span className="big bigRed">
          {amount - remainingInvestment} Kč
        </span>
        <span>&nbsp;z&nbsp;</span>
        <span className="big bigViolet">{amount} Kč</span>
      </div>
      <PercentageIndicator percentage={investmentRate} />
      <div className="text-center">
        <span>Délka splácení&nbsp;</span>
        <span className="big bigRed">{termInMonths}</span>
        <span>&nbsp;měsíců&nbsp;za roční úrok&nbsp;</span>
        <span className="big bigViolet">
          {(interestRate * 100).toFixed(2)} %
        </span>
      </div>
    </React.Fragment>
  );
}

LoanValueIndicator.propTypes = propTypes;
