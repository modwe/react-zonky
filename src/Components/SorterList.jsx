import React from 'react';
import PropTypes from 'prop-types';
import SorterButton, { parameterPropTypes, sorterPropTypes } from './SorterButton';

const propTypes = {
  parameters: PropTypes.arrayOf(PropTypes.shape(parameterPropTypes)).isRequired,
  action: PropTypes.func.isRequired,
  sorter: PropTypes.shape(sorterPropTypes).isRequired,
};

export default function SorterList({ parameters, action, sorter }) {
  if (!parameters || !action) {
    return null;
  }

  const getButtons = () => parameters.map(p =>
    <SorterButton sorter={sorter} key={p.title} parameter={p} action={action} />);

  return (
    <div className="sorterContainer">
      {getButtons()}
    </div>
  );
}

SorterList.propTypes = propTypes;
