import React from 'react';
import PropTypes from 'prop-types';

export const parameterPropTypes = {
  title: PropTypes.string.isRequired,
  parameter: PropTypes.string.isRequired,
};

export const sorterPropTypes = {
  sortBy: PropTypes.string.isRequired,
  orientation: PropTypes.string.isRequired,
};

const propTypes = {
  parameter: PropTypes.shape(parameterPropTypes).isRequired,
  action: PropTypes.func.isRequired,
  sorter: PropTypes.shape(sorterPropTypes).isRequired,
};


export default function SorterButton({ sorter, parameter, action }) {
  const sortASC = () => {
    action({
      by: parameter.parameter,
      orientation: 'ASC',
    });
  };

  const sortDSC = () => {
    action({
      by: parameter.parameter,
      orientation: 'DSC',
    });
  };

  const isSelected = or => sorter.sortBy === parameter.parameter && sorter.orientation === or;

  return (
    <React.Fragment>
      <button className={isSelected('ASC') ? 'selected' : null} onClick={sortASC} >Zoradiť podľa<br />{`${parameter.title} VZ`}</button>
      <button className={isSelected('DSC') ? 'selected' : null} onClick={sortDSC} >Zoradiť podľa<br />{`${parameter.title} ZO`}</button>
    </React.Fragment>
  );
}

SorterButton.propTypes = propTypes;
