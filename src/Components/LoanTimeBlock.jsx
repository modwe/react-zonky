import React from 'react';
import PropTypes from 'prop-types';

const propTypes = {
  sDatePublished: PropTypes.string.isRequired,
  sDeadline: PropTypes.string.isRequired,
};

export default function LoanTimeBlock({ sDatePublished, sDeadline }) {
  const datePublished = new Date(sDatePublished);
  const deadline = new Date(sDeadline);

  return (
    <div className="rowFlex loanTimeBlock">
      <div className="colFlex datePublished">
        <span className="block title">Publikované</span>
        <span className="block time">{`${datePublished.getDate()}.${datePublished.getMonth() + 1}.${datePublished.getFullYear()} ${datePublished.getHours()}:${datePublished.getMinutes()}`}
        </span>
      </div>
      <div className="colFlex remainingHours">
        <span className="block title">Ostáva</span>
        <span className="block time">
          {Math.floor((deadline.valueOf() - datePublished.valueOf()) / 1000 / 60 / 60)}{' '}
          hodin
        </span>
      </div>
      <div className="colFlex deadline">
        <span className="block title">Deadline</span>
        <span className="block time">{`${deadline.getDate()}.${deadline.getMonth() + 1}.${deadline.getFullYear()} ${deadline.getHours()}:${deadline.getMinutes()}`}
        </span>
      </div>
    </div>
  );
}

LoanTimeBlock.propTypes = propTypes;
