export function truncate(text) {
  let truncated = false;
  let words = text.split(' ');

  if (words.length > 20) {
    truncated = true;
    words = words.slice(0, 20);
  }

  const content = words.join(' ');

  if (content.length > 200) {
    truncated = true;
    content.substring(0, 200);
  }

  if (truncated) {
    return `${content}...`;
  }

  return content;
}

export function getZs(n) {
  let s = '';

  for (let i = 0; i < n; i++) {
    s += 'Z';
  }

  return s;
}

export function sort(elems, sortBy, orientation) {
  if (!elems) {
    return null;
  }

  let list = [];

  switch (sortBy) {
    case 'termInMonths':
    case 'amount':
      list = [...elems].sort((a, b) => a[sortBy] - b[sortBy]);
      return orientation === 'ASC' ? list : list.reverse();
    case 'rating':
      list = [...elems].sort((a, b) => {
        const aCompare =
          a[sortBy].length === 5
            ? a[sortBy]
            : a[sortBy] + getZs(5 - a[sortBy].length);
        const bCompare =
          b[sortBy].length === 5
            ? b[sortBy]
            : b[sortBy] + getZs(5 - b[sortBy].length);

        return bCompare.localeCompare(aCompare);
      });

      return orientation === 'ASC' ? list : list.reverse();
    case 'deadline':
      list = [...elems].sort((a, b) => a[sortBy].localeCompare(b[sortBy]));
      return orientation === 'ASC' ? list : list.reverse();
    default:
      return elems;
  }
}
