import axios from 'axios';

const url = 'http://mwd.aspifyhost.sk/api/CORS';
const path = 'https://api.zonky.cz/loans/marketplace';

const getLoans = () => axios.post(url, { path });

const ApiClient = {
  getLoans,
};

export default ApiClient;
